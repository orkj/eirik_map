<?php

header('Content-Type: text/html; charset=utf-8');

function eirik_map_settings($form, &$form_state)
{

  $form['eirik_map_preset_1'] = array(
    '#type' => 'textarea',
    '#title' => t('Preset for object view'),
    '#default_value' => variable_get('eirik_map_preset_1', EIRIK_MAP_PRESET_1),
    '#required' => TRUE,
    '#description' => t('Gmap macro to use in object view. To create your own macro, install GMap Macro builder. If installed, you can click <a href="/map/macro">here</a> to build a macro.')
  );
  $form['eirik_map_preset_2'] = array(
    '#type' => 'textarea',
    '#title' => t('Preset for list view'),
    '#default_value' => variable_get('eirik_map_preset_2', EIRIK_MAP_PRESET_2),
    '#required' => TRUE,
    '#description' => t('Gmap macro to use in list view. To create your own macro, install GMap Macro builder. If installed, you can click <a href="/map/macro">here</a> to build a macro.')
  );
  $form['eirik_map_marker'] = array(
    '#type' => 'textfield',
    '#title' => t('Map marker'),
    '#default_value' => variable_get('eirik_map_marker', EIRIK_MAP_MARKER),
    '#required' => TRUE,
    '#description' => t('Gmap marker to use. If you are not sure what this means - too bad! Try another color if you feel daring.')
  );

  return system_settings_form($form);
}
